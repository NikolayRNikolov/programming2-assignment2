package programming2.assignment2;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class AddNewBoat {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddNewBoat window = new AddNewBoat();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}//end catch exception
			}
		});
	}//end main method

	/**
	 * Create the application.
	 */
        //start constructor AddNewBoat
	public AddNewBoat() {
		initialize();
	}//end constructor AddNewBoat

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}//end method initialize

} //end class AddNewBoat
