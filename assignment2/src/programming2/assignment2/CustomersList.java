/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programming2.assignment2;

/**
 *
 * @author Beh
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

public class CustomersList 
{

	private JFrame frame;
	private JTable table;
	 private String[] columnNames = {"CustomerID","First Name", "Last Name", "Phone Number", "Email", "Street","City", "Postal Code", "Province/State", "Country"	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomersList window = new CustomersList();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomersList() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()  {
		frame = new JFrame();
		frame.setBounds(100, 100, 964, 461);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JScrollPane scrollPane = new JScrollPane();
	      scrollPane.setBounds(10, 42, 884, 495);
             
	     frame.add(scrollPane);
		table = new JTable();
		table.setBounds(10, 11, 888, 413);
                
		frame.getContentPane().add(table);		
       // table.setBorder(UIManager.getBorder("DesktopIcon.border"));
        scrollPane.setViewportView(table);
         
        // the column in the table
       
      ArrayList<Customer> myCustomers=new ArrayList<Customer>();
      Customer c1;
      int  i=0;
      try{
       String filePath=new File("").getAbsolutePath(); 
      //File file=new File("\\src\\programming2\\assignment2\\CustomerData.txt");
       Scanner sc = new Scanner(new File(       
               
               filePath+"\\src\\programming2\\assignment2\\CustomerData.txt"));
       
       while(sc.hasNext()){
           
            
           c1=new Customer(sc.nextInt(),sc.next(),sc.next(),sc.next(),sc.next(),new Address(sc.next(),sc.next(),sc.next(),sc.next(),sc.next()));
           
          myCustomers.add(c1);
          i++;
          System.out.println(c1);
           System.out.println(i);
          
       }
        sc.close();
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                
            },
            new String [] {
                "CustomerID","First Name", "Last Name", "Phone Number", "Email", "Street","City", "Postal Code", "Province/State", "Country"
            }
        ));
        DefaultTableModel model=(DefaultTableModel) table.getModel();
         
       // Object[] row=new Object[i];
        System.out.println("yyyyyy");
       // System.out.println(i);
        for(int j=0;j<i;j++){
            System.out.println("yjjjjjjj");
        //    row[j]={myCustomers.get(j).getCustomerID(),myCustomers.get(j).getfName()};
            Object[] objs = {myCustomers.get(j).getCustomerID(),myCustomers.get(j).getfName(),myCustomers.get(j).getlName(),myCustomers.get(j).getPhoneNo(),
                myCustomers.get(j).getEmail(),myCustomers.get(j).getAdd().getStAddress(),myCustomers.get(j).getAdd().getCity(),
            myCustomers.get(j).getAdd().getPostalCode(),myCustomers.get(j).getAdd().getProvince(),myCustomers.get(j).getAdd().getCountry()};

           
            model.addRow(objs);
           
            
        }
     table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
{
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        c.setBackground(row % 2 == 0 ? Color.LIGHT_GRAY : Color.WHITE);
        return c;
    }
});
        
        
      }catch(Exception e){
          System.out.println(e);
//JOptionPane.showMessageDialog(null,"it has a error ","error", JOptionPane.ERROR);
          
      }
        
       

      

      
       
   
    }
   
  		
		
		

		 
}
