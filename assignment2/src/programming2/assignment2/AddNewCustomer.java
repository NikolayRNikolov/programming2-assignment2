package programming2.assignment2;



import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JButton;

public class AddNewCustomer {

	private JFrame frmMarinaCustomer;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtPhoneNo;
	private JTextField txtEmail;
	private JTextField txtStreet;
	private JTextField txtPostalCode;
        private JTextField txtCity;
        private JTextField txtID;
        private String state;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddNewCustomer window = new AddNewCustomer();
					window.frmMarinaCustomer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddNewCustomer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
                String[] states = { "AL", "AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL" };
                String[] province = { "AB", "BC" ,"MB","NB","NL","NT","NS","NU","ON","PE","QC","SK","YT"};
		
                
            frmMarinaCustomer = new JFrame();
		frmMarinaCustomer.setTitle("Marina New Customer");
		frmMarinaCustomer.setBounds(0, 0, 498, 502);
		frmMarinaCustomer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMarinaCustomer.getContentPane().setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 49, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblFirstName);
		
		txtFirstName = new JTextField();
		txtFirstName.setBounds(107, 49, 155, 24);
		frmMarinaCustomer.getContentPane().add(txtFirstName);
		txtFirstName.setColumns(12);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 84, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblLastName);
		
		txtLastName = new JTextField();
		txtLastName.setColumns(12);
		txtLastName.setBounds(107, 84, 155, 24);
		frmMarinaCustomer.getContentPane().add(txtLastName);
		
		JLabel lblPhoneNo = new JLabel("Phone No");
		lblPhoneNo.setBounds(10, 123, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblPhoneNo);
		
		txtPhoneNo = new JTextField();
		txtPhoneNo.setColumns(12);
		txtPhoneNo.setBounds(107, 123, 155, 24);
		frmMarinaCustomer.getContentPane().add(txtPhoneNo);
		
		JLabel lblEmailAddress = new JLabel("Email Address");
		lblEmailAddress.setBounds(10, 158, 98, 24);
		frmMarinaCustomer.getContentPane().add(lblEmailAddress);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(12);
		txtEmail.setBounds(107, 154, 155, 24);
		frmMarinaCustomer.getContentPane().add(txtEmail);
		
		JLabel lblAdd = new JLabel("Address");
		lblAdd.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAdd.setBounds(10, 198, 111, 24);
		frmMarinaCustomer.getContentPane().add(lblAdd);
		
		JLabel lblStreet = new JLabel("Street");
		lblStreet.setBounds(10, 224, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblStreet);
		
		txtStreet = new JTextField();
		txtStreet.setColumns(12);
		txtStreet.setBounds(56, 224, 401, 24);
		frmMarinaCustomer.getContentPane().add(txtStreet);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setBounds(10, 271, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblCity);
		
		txtCity = new JTextField();
		txtCity.setColumns(12);
		txtCity.setBounds(61, 271, 134, 24);
		frmMarinaCustomer.getContentPane().add(txtCity);
		
		JLabel lblCountry = new JLabel("Country");
		lblCountry.setBounds(10, 320, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblCountry);
		
		
		JLabel lblProvince = new JLabel("Province");
		
		lblProvince.setBounds(10, 364, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblProvince);
		
		JComboBox cmbProvince = new JComboBox(province);
		cmbProvince.setVisible(true);
		cmbProvince.setBounds(97, 366, 111, 20);
		frmMarinaCustomer.getContentPane().add(cmbProvince);
		
		
		JComboBox cmbStates = new JComboBox(states);
		cmbStates.setVisible(false);
		cmbStates.setBounds(97, 366, 111, 20);
		frmMarinaCustomer.getContentPane().add(cmbStates);
		
		
		String[] countryStr = { "Canada", "US" };
		
		
		JComboBox cmbCountry = new JComboBox(countryStr);
		cmbCountry.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {				
				if(cmbCountry.getSelectedIndex()==1){
                                    cmbStates.setVisible(true);cmbProvince.setVisible(false);
                                    lblProvince.setText("State");
                                    state=cmbStates.getSelectedItem().toString();
                                }
				if(cmbCountry.getSelectedIndex()==0){
                                    cmbProvince.setVisible(true); 
                                    cmbStates.setVisible(false);
                                    lblProvince.setText("Province");
                                     state=cmbProvince.getSelectedItem().toString();
                                }
			}
		});
		
		cmbCountry.setBounds(97, 322, 111, 20);
		
		
		frmMarinaCustomer.getContentPane().add(cmbCountry);
		
		JLabel lblPostalCode = new JLabel("Postal Code");
		lblPostalCode.setBounds(242, 271, 98, 24);
		frmMarinaCustomer.getContentPane().add(lblPostalCode);
		
		txtPostalCode = new JTextField();
		txtPostalCode.setColumns(12);
		txtPostalCode.setBounds(323, 271, 134, 24);
		frmMarinaCustomer.getContentPane().add(txtPostalCode);
		
		JButton btnNewButton = new JButton("Add New Customer");
		btnNewButton.setBounds(76, 409, 146, 35);
		frmMarinaCustomer.getContentPane().add(btnNewButton);               
		btnNewButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
                    int CustId=0;
		  try{
                        String filePath=new File("").getAbsolutePath();       
                         Scanner sc = new Scanner(new FileReader(
                                                      filePath+"\\src\\programming2\\assignment2\\CustomerIDCreater.txt"));
			  CustId=sc.nextInt();
                          sc.close();
                          CustId++;
                          txtID.setText(String.format("%d",CustId));
			FileWriter outFile=new FileWriter(filePath+"\\src\\programming2\\assignment2\\CustomerData.txt",true);
			 Customer cust;
			  Address add;
                          add=new Address(txtStreet.getText(),txtCity.getText(),state , txtPostalCode.getText(),cmbCountry.getSelectedItem().toString() );
                          System.out.println(add.toString());
                          cust=new Customer(CustId, txtFirstName.getText(), txtLastName.getText(), txtEmail.getText(), txtPhoneNo.getText(), add);
                          System.out.println(cust.toString());
			  outFile.write(cust.toString());
                          outFile.write("\r\n");
                          outFile.close();
                         
                            
                          
			PrintWriter pw=new PrintWriter(filePath+"\\src\\programming2\\assignment2\\CustomerIDCreater.txt");    
                         pw.println(CustId);
                         System.out.println(CustId);
                      pw.close();
			      
                     }
                                catch(Exception e){
                                    System.out.println(e.toString());
                                }
			       
				
			}
		});
                
                
                
                
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(257, 409, 125, 35);
		frmMarinaCustomer.getContentPane().add(btnReset);
		
		JLabel lblCustomerid = new JLabel("CustomerID");
		lblCustomerid.setBounds(10, 11, 77, 24);
		frmMarinaCustomer.getContentPane().add(lblCustomerid);
		
		txtID = new JTextField();
		txtID.setEditable(false);
		txtID.setColumns(12);
		txtID.setBounds(107, 11, 155, 24);
		frmMarinaCustomer.getContentPane().add(txtID);
		
	
		

            
		
		
		
	
		
	}
}
